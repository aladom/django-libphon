==============
Django Libphon
==============

Phone app for Django


## Install


Last version

`pip install -e git+https://gitlab.com/aladom/django-libphon.git#egg=django_libphon`

Specific version

`pip install -e git+https://gitlab.com/aladom/django-libphon.git@{commit_hash}#egg=django_libphon`


## Quick start

Add "libphon" to your INSTALLED_APPS setting like this:

   ```python
   INSTALLED_APPS = [
        'libphon',

        'django.contrib.auth',
        'django.contrib.contenttypes',
   ]
   ```

Backends
--------

- `Octopush`
- `Digitaleo`
