# -*- coding: utf-8 -*-
from celery import shared_task

from .sms.backends import get_backend


@shared_task
def send_sms(phone, message, send_date=None, first_name=None, last_name=None,
             civility=None):
    SMS = get_backend()
    sms = SMS(
        message, phone, send_date=send_date, from_header=None,
        first_name=first_name, last_name=last_name, civility=civility
    )
    sms.send()
    return sms
