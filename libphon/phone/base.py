# -*- coding: utf-8 -*-
# Copyright (c) 2016 Aladom SAS & Hosting Dvpt SAS
from .types import get_phone_type, clean_number

from ..sms.backends import get_backend as get_sms_backend

__all__ = [
    'Phone',
]


class Phone:

    _first_name = None
    _last_name = None
    _civility = None

    @property
    def value(self):
        return self.get_cleaned_value()

    @value.setter
    def value(self, value):
        self._type = get_phone_type(value)
        self._value = value

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, value):
        self._first_name = value

    @property
    def last_name(self):
        return self._last_name

    @last_name.setter
    def last_name(self, value):
        self._last_name = value

    @property
    def civility(self):
        return self._civility

    @civility.setter
    def civility(self, value):
        self._civility = value

    def __init__(self, value, first_name=None, last_name=None, civility=None):
        self.value = value
        self.first_name = first_name
        self.last_name = last_name
        self.civility = civility

    def __eq__(self, other):
        if isinstance(other, str):
            other = self.__class__(other)
        if not isinstance(other, self.__class__):
            return False
        return self.value == other.value

    def __str__(self):
        return self._value or ''

    def __len__(self):
        return len(self._value)

    def is_valid(self):
        return self._type is not None

    def is_mobile(self):
        return self.is_valid() and self._type.is_mobile(self._value)

    def get_country(self):
        if self.is_valid():
            return self._type.country_code
        return None

    def get_cleaned_value(self):
        if self.is_valid():
            return self._type.clean(self._value)
        return clean_number(self._value)

    def format(self, separator=None, international=True):
        if self.is_valid():
            return self._type.format(self._value, separator, international)
        return self.value

    def local_format(self, separator=None):
        return self.format(separator, international=False)

    def send_sms(self, message, **kwargs):
        backend = kwargs.pop('backend', get_sms_backend())
        kwargs = self.populate_kwargs(kwargs)
        sms = backend(message, self, **kwargs)
        sms.send()

    def send_sms_async(self, message, **kwargs):
        """Send SMS asynchroneously. Requires Celery."""
        try:
            from ..tasks import send_sms
        except ImportError:
            raise ImportError(
                "This service requires Celery. You can install it with: "
                "pip install celery"
            )
        kwargs = self.populate_kwargs(kwargs)
        send_sms.delay(self.value, message, **kwargs)

    def populate_kwargs(self, kwargs):
        if self.first_name and 'first_name' not in kwargs:
            kwargs['first_name'] = self.first_name
        if self.last_name and 'last_name' not in kwargs:
            kwargs['last_name'] = self.last_name
        if self.civility and 'civility' not in kwargs:
            kwargs['civility'] = self.civility
        return kwargs
