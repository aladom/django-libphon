# -*- coding: utf-8 -*-
# Copyright (c) 2016 Aladom SAS & Hosting Dvpt SAS
import json
import logging
import re
import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

from django.conf import settings
from django.http import QueryDict
from django.utils.module_loading import import_string

import requests

from ..conf import (
    SMS_API_KEY, SMS_BACKEND, SMS_DEFAULT_FROM, DEV_PHONES,
    SMS_LOGIN, SMS_TYPE, SMS_SENDER
)
from ..exceptions import (
    PhoneError, InvalidPhoneNumber, NotAMobilePhone, ServiceUnavailable,
)

__all__ = [
    'Backend', 'Digitaleo', 'Mailjet', 'UndefinedBackend', 'get_backend',
    'OctoPush'
]


char_count_double = '^\\|~[]{}€\x0c'
sms_max_length = 160
long_sms_max_length = 1024
long_sms_single_max_length = 153

logger = logging.getLogger('libphon.sms')


class Backend:

    def __init__(self, message, phone, send_date=None, from_header=None,
                 first_name=None, last_name=None, civility=None):
        self.message = message
        if send_date:
            self.send_date = send_date.replace(microsecond=0)
        else:
            self.send_date = None
        if settings.DEBUG and DEV_PHONES and phone not in DEV_PHONES:
            phone = DEV_PHONES[0]
        if isinstance(phone, str):
            from ..phone import Phone  # import here to avoid circular imports
            phone = Phone(phone)
        self.phone = phone
        self.from_header = from_header or SMS_DEFAULT_FROM
        self.first_name = first_name
        self.last_name = last_name
        self.civility = civility

    def get_length(self):
        return len(self.message) + sum(self.message.count(c)
                                       for c in char_count_double)

    def get_nb_sms(self):
        length = self.get_length
        if length <= sms_max_length:
            return 1
        else:
            return length // long_sms_single_max_length


class UndefinedBackend(Backend):

    def __init__(self, *args, **kwargs):
        raise NotImplementedError("You must configure an SMS backend "
                                  "in order to send SMS messages.")


class Digitaleo(Backend):

    send_url = 'https://www.ecosms.fr/ecosms.php'
    response_expr = re.compile('^([a-z_]+)=(.*)$', re.M)

    def __init__(self, message, phone, send_date=None, from_header=None):
        if from_header:
            logger.warning("from_header not supported by Digitaleo backend.")
        return super().__init__(message, phone, send_date, None)

    def send(self):
        """Send the SMS through Digitaleo API.
        May raise `InvalidPhoneNumber` if the phone number is not valid.
        May raise `NotAMobilePhone` if the phone number is not detected as
        mobile phone.
        May raise `ServiceUnavailable` if an error occurs when attempting to
        reach Digitaleo API.
        May raise `PhoneError` if something else failed with Digitaleo.
        """
        if not self.phone.is_valid():
            raise InvalidPhoneNumber(self.phone)
        if not self.phone.is_mobile():
            raise NotAMobilePhone(self.phone)
        querydict = QueryDict(mutable=True)
        querydict['sms'] = self.message
        querydict['mobile'] = self.phone.value
        querydict['code'] = SMS_API_KEY
        querydict['charset'] = 'UTF-8'
        if self.send_date:
            querydict['date'] = self.send_date.isoformat()
        url = '{}?{}'.format(self.send_url, querydict.urlencode())
        response = requests.get(url)
        if response.status_code >= 500:
            raise ServiceUnavailable(response.text)
        self.parse_response(response)
        if self.get_status() == 'ko':
            raise PhoneError(self.response)

    def parse_response(self, response):
        self.response = dict(self.response_expr.findall(response.text))

    def get_status(self):
        if 'statut' in self.response:
            return self.response['statut'].split(' ', 1)[0]
        else:
            return 'ko'

    def get_status_message(self):
        if 'statut' in self.response:
            return self.response['statut'].split(' ', 1)[1]
        else:
            return ''

    def get_sms_id(self):
        return self.response.get('sms_id', None)


class OctoPush(Backend):

    send_url = 'https://api.octopush.com/v1/public/sms-campaign/send'
    response = None

    def __init__(self, message, phone, send_date=None, from_header=None,
                 first_name=None, last_name=None, civility=None):
        if from_header:
            logger.warning("from_header not supported by Octopush backend.")
        super().__init__(message, phone, send_date, None,
                         first_name, last_name, civility)

    def send(self):
        """Send the SMS through OctoPush API.
        May raise `InvalidPhoneNumber` if the phone number is not valid.
        May raise `NotAMobilePhone` if the phone number is not detected as
        mobile phone.
        May raise `ServiceUnavailable` if an error occurs when attempting to
        reach OcotoPush API.
        May raise `PhoneError` if something else failed with OctoPush.
        """
        if not self.phone.is_valid():
            raise InvalidPhoneNumber(self.phone)
        if not self.phone.is_mobile():
            raise NotAMobilePhone(self.phone)
        recipient = {"phone_number": self.phone.value}
        if self.phone.first_name or self.first_name:
            recipient['first_name'] = self.phone.first_name or self.first_name
        if self.phone.last_name or self.last_name:
            recipient['last_name'] = self.phone.last_name or self.last_name
        if self.phone.civility or self.civility:
            recipient['param1'] = self.phone.civility or self.civility
        params = {
            "recipients": [recipient], "purpose": "alert",
            "text": self.message, "sender": SMS_SENDER,
        }
        headers = {
            'Content-Type': 'application/json',
            'api-login': SMS_LOGIN,
            'api-key': SMS_API_KEY,
            'cache-control': 'no-cache',
        }
        payload = json.dumps(params)

        session = requests.Session()
        retry = Retry(connect=3, backoff_factor=0.5)
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)

        response = session.post(self.send_url, data=payload, headers=headers)
        if response.status_code >= 500:
            raise ServiceUnavailable(response.text)
        self.parse_response(response)
        # 201 on doc but API return 200...
        if response and response.status_code not in [200, 201]:
            raise PhoneError(self.response)

    def parse_response(self, response):
        self.response = response.json()

    def get_sms_id(self):
        return self.response.get('ticket', None)


class Mailjet(Backend):

    send_url = 'https://api.mailjet.com/v4/sms-send'

    def __init__(self, message, phone, send_date=None, from_header=None):
        if send_date:
            logger.warning("send_date not supported by Mailjet backend.")
        return super().__init__(message, phone, None, from_header)

    def send(self):
        """Send the SMS through Mailjet API.
        May raise `InvalidPhoneNumber` if the phone number is not valid.
        May raise `NotAMobilePhone` if the phone number is not detected as
        mobile phone.
        May raise `ServiceUnavailable` if an error occurs when attempting to
        reach Mailjet API.
        May raise `PhoneError` if something else failed with Mailjet.
        """
        if not self.phone.is_valid():
            raise InvalidPhoneNumber(self.phone)
        if not self.phone.is_mobile():
            raise NotAMobilePhone(self.phone)
        request_payload = {
            'From': self.from_header,
            'To': self.phone.format(separator='', international=True),
            'Text': self.message,
        }
        request_headers = {'Authorization': 'Bearer {}'.format(SMS_API_KEY)}
        response = requests.post(
            self.send_url, json=request_payload, headers=request_headers)
        if response.status_code >= 500:
            raise ServiceUnavailable(response.text)
        self.parse_response(response)
        if self.get_status() != 2:
            raise PhoneError(self.response)

    def parse_response(self, response):
        self.response = response.json()

    def get_status(self):
        try:
            return self.response['Status']['Code']
        except KeyError:
            return -1

    def get_status_message(self):
        try:
            return self.response['Status']['Description']
        except KeyError:
            return ''

    def get_sms_id(self):
        return self.response.get('MessageId', None)


def get_backend():
    return import_string(SMS_BACKEND)
